Neocities DNS
=============

This script generates a HTML file with a link to the public IP address of the
machine it's running on. The file is then uploaded to neocities.org, allowing
anyone who knows the URL of the uploaded page to easily access the machine as
a web server - essentially turning Neocities into a Dynamic DNS service.

Running this script as a cron job is a good idea, but please don't run it too
frequently; a request is made to <http://ifconfig.me> each time, and constantly
spamming their server with requests wouldn't be very polite. :)

Please note that this script is neither affiliated with nor endorsed by either
<http://ifconfig.me> or <https://neocities.org> - it simply makes use of their
public APIs.

License
-------

This code is licensed as [CC0](http://creativecommons.org/publicdomain/zero/1.0)
i.e. it is placed in the public domain. Use or modify it as you see fit.
