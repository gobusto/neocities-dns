#!/bin/bash

# This script generates a HTML file with a link to the public IP address of the
# machine it's running on. The file is then uploaded to neocities.org, allowing
# anyone who knows the URL of the uploaded page to easily access the machine as
# a web server - essentially turning Neocities into a Dynamic DNS service.
#
# Running this script as a cron job is a good idea, but please don't run it too
# frequently; a request is made to http://ifconfig.me each time, and constantly
# spamming their server with requests wouldn't be very polite. :)

html=server.htm
user=yourname
pass=password

# First, we need to get the public IP address of this machine. We can't rely on
# local information, since we might be on a LAN/behind a proxy server/etc.

url=http://$(curl --silent ifconfig.me):3000

# Now that we know the IP address, we can generate a new HTML document:

temphtml=$(mktemp)
echo "<!DOCTYPE html>" >> $temphtml
echo "<head>" >> $temphtml
echo "<meta charset='utf-8'>" >> $temphtml
echo "<meta http-equiv='refresh' content='5; url=$url'>" >> $temphtml
echo "<title>Redirecting...</title>" >> $temphtml
echo "</head>" >> $temphtml
echo "<body>" >> $temphtml
echo "<p>You should be redirected in 5 seconds. If not, click" >> $temphtml
echo "<a href='$url'>here</a>.</p>" >> $temphtml
echo "</body>" >> $temphtml
echo "</html>" >> $temphtml

# Now upload the file to Neocities, but ONLY if it differs from the old version
# generated by the previous run. This is to avoid needlessly re-uploading it to
# Neocities; simply delete the old file if you need to force a new upload.

old_file="/var/tmp/$html"
if $(diff "$temphtml" $old_file &> /dev/null); then
	echo "No differences found; file not uploaded."
else
	echo "Uploading new version..."
	result=$(curl --silent -F $html=@"$temphtml" https://$user:$pass@neocities.org/api/upload)
	echo $result

	# If everything went well, update the "reference" copy.
	if [[ $result == *"success"* ]]; then
		cp "$temphtml" $old_file
	fi
fi
